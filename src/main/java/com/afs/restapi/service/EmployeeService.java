package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeCreatedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmployeeService {
    public static final String EMPLOYEE_AGE_RANGE_ERROR_MESSAGE = "Employee must be 18~65 years old";
    public static final String EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE = "Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created";
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee){
        if (employee.getAge() <= 17 || employee.getAge() >= 66)
            throw new EmployeeCreatedException(EMPLOYEE_AGE_RANGE_ERROR_MESSAGE);
        if (employee.getAge() >= 30 && employee.getSalary() < 20000)
            throw new EmployeeCreatedException(EMPLOYEE_AGE_AND_SALARY_ERROR_MESSAGE);
        return employeeRepository.insert(employee);
    }

    public void delete(Long id) {
        Employee toRemovedEmployee = employeeRepository.findById(id);
        toRemovedEmployee.setActiveStatus(false);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return  employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee update(Long id, Employee employee) {
        return employeeRepository.update(id, employee);
    }
}
