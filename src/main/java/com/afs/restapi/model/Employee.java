package com.afs.restapi.model;

import java.util.Objects;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
    private Long companyId;
    private boolean activeStatus;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return activeStatus == employee.activeStatus && Objects.equals(id, employee.id) && Objects.equals(name, employee.name) && Objects.equals(age, employee.age) && Objects.equals(gender, employee.gender) && Objects.equals(salary, employee.salary) && Objects.equals(companyId, employee.companyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, gender, salary, companyId, activeStatus);
    }

    public Employee() {
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.activeStatus = true;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId) {
        this(id, name, age, gender, salary);
        this.companyId = companyId;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId, boolean activeStatus) {
        this(id, name, age, gender, salary);
        this.companyId = companyId;
        this.activeStatus = activeStatus;
    }

    public boolean getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public void merge(Employee employee) {
        this.salary = employee.getSalary();
        this.age = employee.getAge();
        this.companyId = employee.getCompanyId();
    }


}
