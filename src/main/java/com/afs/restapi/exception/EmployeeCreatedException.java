package com.afs.restapi.exception;

public class EmployeeCreatedException extends RuntimeException {

    public EmployeeCreatedException(String message) {
        super(message);
    }
}
