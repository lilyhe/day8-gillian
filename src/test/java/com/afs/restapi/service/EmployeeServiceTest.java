package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeCreatedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class EmployeeServiceTest {
    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(employeeRepository);


    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    // @Test
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "female", 20000);

        when(employeeRepository.insert(eq(employee)))
                .thenReturn(new Employee(1L, "Lily", age, "female", 20000));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
    }


    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    // @Test
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {
        //given
        Employee employee = new Employee(null, "Lily", age, "female", 20000);
        EmployeeCreatedException employeeCreatedException = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employee must be 18~65 years old", employeeCreatedException.getMessage());

    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30, 20000", "29, 19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) {
        //given
        Employee employee = new Employee(null, "Lily", age, "female", salary);

        when(employeeRepository.insert(employee))
                .thenReturn(new Employee(1L, "Lily", age, "female", salary));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());

    }

    @Test
    void should_throw_exception_when_create_employee_given_age_is_30_and_salary_is_19999() {
        //given
        Employee employee = new Employee(null, "Lily", 30, "female", 19999);
        EmployeeCreatedException employeeCreatedException = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created", employeeCreatedException.getMessage());

    }

    @Test
    void should_return_created_employee_with_true_active_status_when_create_employee_given_employee_age_is_between_18_to_65() {
        Employee employee = new Employee(null, "Lily", 19, "female", 20000);

        when(employeeRepository.insert(eq(employee)))
                .thenReturn(new Employee(1L, "Lily", 19, "female", 20000));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(19, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
        assertTrue(employeeResponse.getActiveStatus());
    }

    @Test
    void should_set_active_status_false_when_delete_employee_given_id() {
        Employee employee = new Employee(1L, "Lily", 19, "female", 20000);

        when(employeeRepository.findById(eq(1L))).thenReturn(employee);
        employeeService.delete(employee.getId());
        assertFalse(employee.getActiveStatus());
    }

    @Test
    void should_reject_to_update_when_update_employee_given_employee_left_the_company() {
        Employee employee = new Employee(1L, "Lily", 19, "Female", 20000, null, false);

        when(employeeRepository.update(1L, employee)).
                thenReturn(new Employee(1L, "Lily", 20, "Female", 2000, null, false));

        Employee updatedEmploy = employeeService.update(1L, employee);
        assertNotNull(employee);
        assertEquals(1L, updatedEmploy.getId());
        assertEquals("Lily", updatedEmploy.getName());
        assertEquals(20, updatedEmploy.getAge());
        assertEquals("Female", updatedEmploy.getGender());
        assertEquals(2000, updatedEmploy.getSalary());
        assertNull(updatedEmploy.getCompanyId());
        assertFalse(updatedEmploy.getActiveStatus());
    }

    @Test
    void should_return_all_employees_when_perform_get_all_employee_given_nothing() {
        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1L, "Lily", 20, "Female", 2000));
        employees.add(new Employee(2L, "Lucy", 19, "Female", 20000));

        when(employeeRepository.findAll())
                .thenReturn(employees);

        List<Employee> employeesResponse = employeeService.findAll();

        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.get(0), employees.get(0));
        assertEquals(employeesResponse.get(1), employees.get(1));
    }

    @Test
    void should_return_employee_when_perform_get_employee_given_id() {
        when(employeeRepository.findById(1L))
                .thenReturn(new Employee(1L, "Lily", 20, "Female", 2000));

        Employee employee = employeeService.findById(1L);

        assertNotNull(employee);
        assertEquals(1L, employee.getId());
        assertEquals("Lily", employee.getName());
        assertEquals(20, employee.getAge());
        assertEquals("Female", employee.getGender());
        assertEquals(2000, employee.getSalary());
    }

    @Test
    void should_return_employees_when_perform_get_employees_given_gender() {
        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1L, "Lily", 20, "Female", 2000));
        employees.add(new Employee(2L, "Lucy", 19, "Female", 20000));

        when(employeeRepository.findByGender("Female"))
                .thenReturn(employees);

        List<Employee> employeesResponse = employeeService.findByGender("Female");

        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.get(0), employees.get(0));
        assertEquals(employeesResponse.get(1), employees.get(1));
    }

    @Test
    void should_return_employees_when_perform_get_employees_given_page_and_size() {
        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee(1L, "Lily1", 20, "Female", 8000));
        employees.add(new Employee(2L, "Lily2", 20, "Female", 8000));
        employees.add(new Employee(3L, "Lily3", 20, "Female", 8000));
        employees.add(new Employee(4L, "Lily4", 20, "Female", 8000));
        employees.add(new Employee(5L, "Lily5", 20, "Female", 8000));
        employees.add(new Employee(6L, "Lily6", 20, "Female", 8000));

        when(employeeRepository.findByPage(1, 2))
                .thenReturn(employees.subList(0, 2));

        List<Employee> employeesResponse = employeeService.findByPage(1,2);

        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.size(), 2);
        assertEquals(employeesResponse.get(0), employees.get(0));
        assertEquals(employeesResponse.get(1), employees.get(1));
    }


}
