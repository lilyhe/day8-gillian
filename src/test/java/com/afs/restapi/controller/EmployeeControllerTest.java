package com.afs.restapi.controller;

import com.afs.restapi.model.Employee;
import com.afs.restapi.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc client;

    @MockBean
    private EmployeeService employeeService;  // 因为service依赖于repository，repo可能还依赖于别的很多东西，但我们只想测service，所以Mock。预期Mock这个替身的动作永远正确

    @Autowired
    private ObjectMapper mapper;

    @Test
    void should_call_service_when_perform_post_employee_given_employee() throws Exception {
        Employee employee = new Employee(null, "name", 20, "male", 2000);
        String requestJson = mapper.writeValueAsString(employee);

        client.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated());

        verify(employeeService).create(employee);

    }

    @Test
    void should_set_status_false_when_perform_delete_given_employee() throws Exception {
        Employee employee = new Employee(1L, "name", 20, "male", 2000);

        client.perform(delete("/employees/{id}", 1L)).andExpect(status().isNoContent());

        verify(employeeService).delete(1L);

    }
}
