package com.afs.restapi.integration;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyIntegrationTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    private static Company buildCompanySpring() {
        return new Company(1L, "Spring");
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        //given
        Company newCompany = buildCompanySpring();
        companyRepository.addCompany(newCompany);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Spring"));
    }

    @Test
    void should_return_company_spring_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company company1 = buildCompanySpring();
        Company company2 = buildCompanyBaidu();
        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Spring"));
    }

    private static Company buildCompanyBaidu() {
        return new Company(2L, "Baidu");
    }

    private static Company buildCompanyBoot() {
        return new Company(3L, "Boot");
    }

    @Test
    void should_return_one_page_companies_when_perform_get_by_page_given_companies_in_repo() throws Exception {

        //given
        Company company1 = buildCompanySpring();
        Company company2 = buildCompanyBaidu();
        Company company3 = buildCompanyBoot();

        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);
        companyRepository.addCompany(company3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageNumber=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Spring"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Baidu"));
    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company companySpring = buildCompanySpring();
        String springJson = mapper.writeValueAsString(companySpring);
        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(springJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Spring"));
        Company companySaved = companyRepository.findById(1L);
        assertEquals(companySpring.getId(), companySaved.getId());
        assertEquals(companySpring.getName(), companySaved.getName());
    }

    @Test
    void should_update_company_in_repo_when_perform_put_by_id_given_company_in_repo_and_update_info() throws Exception {

        //given
        Company company1 = buildCompanySpring();
        companyRepository.addCompany(company1);
        Company toBeUpdateSpring = buildCompanySpring();
        toBeUpdateSpring.setName("Summer");
        String springson = mapper.writeValueAsString(toBeUpdateSpring);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(springson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Summer"));
        Company springInRepo = companyRepository.findById(1L);
        assertEquals(toBeUpdateSpring.getName(), springInRepo.getName());
    }

    @Test
    void should_del_company_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company1 = buildCompanySpring();
        companyRepository.addCompany(company1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(NotFoundException.class, () -> companyRepository.findById(1L));
    }

}
