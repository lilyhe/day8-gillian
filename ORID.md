# day8-Gillian

## Objective: 
Today, we continued with springboot. I focused on the three layers of springboot architecture: Controller-Presentation Layer, Service-Business Layer and Repository-Persistance Layer. This architecture makes the functional division clearer.
We then complete the exercise using the idea of TDD. Separate test for each layer is a must to ensure that each layer functions properly.

## Reflective: 
Learn a lot but very difficult

## Interpretive: 
I am not familiar with the three layers, so it is often unclear which layer some AC should be implemented in and the test cases will be confused. Teachers wrote the code directly instead of introducing related concepts, which was hard for me to follow.

## Decisional: 
Review the exercises in class and distinguish the role of the three layers.











